// this is secret key to access TokBox by param get on secret-key.js
var apiKey = API_KEY;
var sessionId = SESSION_ID;
var token = TOKEN;

// initialize variable
var session;
var plusSubscriber = 1;
var nameSubscriber;

// count of pariticpant stream
var connectionCount = 0;

// get data from live blade
var role_user = $('#role').val();
var fullname = $('#fullname').val();


initializeSession();

// Handling all of our errors here by alerting them
function handleError(error) {
    if (error) {
        alert(error.message);
    }
}

function initializeSession() {

    if (OT.checkSystemRequirements() == 1) {
        session = OT.initSession(apiKey, sessionId);
    } else {
        // The client does not support WebRTC.
        // You can display your own message.
        Swal.fire({
            title: 'Browser Tidak Mendukung!',
            text: 'Agar aplikasi berjalan dengan lancar mohon gunakan browser Google Chrome.',
            type: 'warning',
            confirmButtonText: 'OK'
        })
    }

    // event to get connection created, destroyed and check client connection
    session.on({
        connectionCreated: function (event) {
            connectionCount++;
            console.log(connectionCount + ' connections created.');
            $("#connection-count").text(connectionCount);
            $("#current-user").html(
                '<div class="col-md-10 p-0">' + fullname + '</div>'
            );
        },
        connectionDestroyed: function (event) {
            connectionCount--;
            console.log(connectionCount + ' connections destroyed.');
            $("#connection-count").text(connectionCount);
        },
        sessionDisconnected: function sessionDisconnectHandler(event) {
            // The event is defined by the SessionDisconnectEvent class
            console.log('Disconnected from the session.');
            // document.getElementById('disconnectBtn').style.display = 'none';
            if (event.reason == 'networkDisconnected') {
                // alert('Your network connection terminated.')
                Swal.fire({
                    title: 'Jaringan Internet Bermasalah!',
                    text: 'Mohon periksa jaringan internet anda, atau coba aktifkan ulang paket data anda.',
                    type: 'warning',
                    confirmButtonText: 'OK'
                })
            }
        }
    });

    // Subscribe to a newly created stream
    session.on('streamCreated', function(event) {
        session.subscribe(event.stream, 'subscriberContainer'+plusSubscriber, {
            insertMode: 'replace',
            width: '100%',
            height: '100%',
            name: event.stream.name
        }, handleError);
        plusSubscriber++;
        console.log(event.stream);
        $('#other-user').append(
            '<li class="other-user">' +
            '<div class="row align-items-center m-3">' +
            '<div class="col-md-2 p-0">' +
            '<div class="sender-img">' +
            '<img src="http://nicesnippets.com/demo/image1.jpg" class="float-left">' +
            '</div>' +
            '</div>' +
            '<div class="col-md-10 p-0">'+
            event.stream.name +
            '</div>'+
            '</div>'+
            '</li>'
        );
    });

    // Create a publisher
    var publisher = OT.initPublisher('publisherContainer', {
        insertMode: 'replace',
        width: '100%',
        height: '100%',
        name: fullname,
    }, handleError);

    // Connect to the session
    session.connect(token, function(error) {
        // If the connection is successful, publish to the session
        if (error) {
            handleError(error);
        } else {
            session.publish(publisher, handleError);
        }
    });
}