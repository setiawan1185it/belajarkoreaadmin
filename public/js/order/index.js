// $($document).ready(function(){
    function buttonReject(id) {
        swal.fire({
            title: 'Apakah anda menolak?',
            text: "Anda akan menolak order ini!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#9c9c9c',
        }).then(function(result) {
            $('.do-reject').append('<input type="hidden" name="order_id" value="'+id+'">');
            if (result.value) {
                $('.do-reject').submit();
            }
        });
    }
    
    function buttonConfirm(id) {
        swal.fire({
            title: 'Konfirmasi order?',
            text: "Anda akan mengkonfirmasi bahwa order ini sudah sesuai!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#9c9c9c',
        }).then(function(result) {
            $('.do-confirm').append('<input type="hidden" name="order_id" value="'+id+'">');
            if (result.value) {
                $('.do-confirm').submit();
            }
        });
    }
// });