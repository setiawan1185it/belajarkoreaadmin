let mediaId = ($('.media-question').find(".media-file")).length;

$('#media_url').change(function () {
    if($('#media-type').val()== 'image'){
        $('#media_preview').attr('src', $(this).val());
    }else{
        $('#media_preview_audio').attr('src', $(this).val());
    }
});

const mediatype = $('#media-type').val();
const elMedia = $('.media');
const elMediaImage = $('.media-image');
const elMediaAudio = $('.media-audio');

elMediaImage.hide();
elMediaAudio.hide();
elMedia.hide();

if(mediatype == 'media') {
    elMedia.show();
} 

$('#media-type').change(function () {
    elMedia.hide();
    $('#error-text').text('');
    $('.media-question').find('.media-item').remove();
    const mediatype = $(this).val();
    mediaId = 0;
    
    if(mediatype == 'media') {
        elMedia.show();
    }else{
        // $('#media_url').val('');
    }
});

$('#addAudio').click(function(){
    $('#error-media').text('');
    if(!checkMediaFile()) {
        $('#error-media').text('Silahkan upload file terlebih dahulu!');
        const scrollPos = $('#error-media').offset().top - 170;
        $('html, body').animate({
            scrollTop: scrollPos
        }, 1500);
        return false;
    }
    if (!checkMaxField(4)) {
        const scrollPos = $('#error-media').offset().top - 170;
        $('html, body').animate({
            scrollTop: scrollPos
        }, 1500);
        return false;
    }
    mediaId++;
    let mediaAppend = '';
    mediaAppend = `
    <div class="col-lg-6 mb-3 media-item" id="mediaItem`+mediaId+`">
        <button type="button" id="btnMedia`+mediaId+`" onclick="uploadMedia('mediaFile`+mediaId+`')" class="btn btn-outline-brand btn-elevate btn-icon mb-3" title="Uplaod File"><i class="la la-cloud-upload"></i></button>
        <input type="file" class="media-file" name="media_file[]" id="mediaFile`+mediaId+`" onchange="previewMedia(this, 'media_preview`+mediaId+`', 'audio')" value="{{ old('media_file') }}" accept="audio/mp3" hidden>
        <input type="hidden" name="media_file_type[]" value="audio">
        <input type="hidden" name="media_file_id[]" value="new">
        <div>
            <div class="kt-avatar kt-avatar--outline kt-avatar--circle-"  style="width: auto">
                <audio controls class="embed-responsive-item media-preview" id="media_preview`+mediaId+`" src="" ></audio>
                <label class="kt-avatar__upload" title="" data-original-title="Delete media" id="removeMedia`+mediaId+`" onclick="removeMedia('mediaItem`+mediaId+`')">
                    <i class="fa fa-trash"></i>
                </label>
            </div>
        </div>
    </div>
    `;
    $('.media-question').append(mediaAppend);
});

$('#addImage').click(function(){
    $('#error-media').text('');
    if(!checkMediaFile()) {
        $('#error-media').text('Silahkan upload file terlebih dahulu!');
        const scrollPos = $('#error-media').offset().top - 170;
        $('html, body').animate({
            scrollTop: scrollPos
        }, 1500);
        return false;
    }
    if (!checkMaxField(4)) {
        const scrollPos = $('#error-media').offset().top - 170;
        $('html, body').animate({
            scrollTop: scrollPos
        }, 1500);
        return false;
    }
    mediaId++;
    let mediaAppend = '';
    mediaAppend = `
    <div class="col-lg-6 mb-3 media-item" id="mediaItem`+mediaId+`">
        <button type="button" id="btnMedia`+mediaId+`" onclick="uploadMedia('mediaFile`+mediaId+`')" class="btn btn-outline-brand btn-elevate btn-icon mb-3" title="Uplaod File"><i class="la la-cloud-upload"></i></button>
        <input type="file" class="media-file" name="media_file[]" id="mediaFile`+mediaId+`" onchange="previewMedia(this, 'media_preview`+mediaId+`', 'image')" accept="image/jpeg,image/gif,image/png" hidden>
        <input type="hidden" name="media_file_type[]" value="image">
        <input type="hidden" name="media_file_id[]" value="new">
        <div>
            <div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_apps_user_add_avatar" style="width: 75%">
                <img class="kt-avatar__holder rounded media-preview" id="media_preview`+mediaId+`" src=""  onerror="this.src='/img/no-thumbnail.png'" style="width: 100%; height: auto;">
                <label class="kt-avatar__upload" title="" data-original-title="Delete media" id="removeMedia`+mediaId+`" onclick="removeMedia('mediaItem`+mediaId+`')">
                    <i class="fa fa-trash"></i>
                </label>
            </div>
        </div>
    </div>
    `;
    $('.media-question').append(mediaAppend);
});





const type = $('#type').val();
$('#type').change(function () {
    if($(this).val() == 'stacking-word') {
        $('#word-answer').show();
    } else {
        $('#word-answer').hide();
    }
});



$('#answers-type').change(function(){
    $('#list-answer').find('tr').remove();
    fieldId = 0;
})


$('#add-answer').click(function () {
    $('#error-text').text('');
    if(!checkAnswerText()) {
        $('#error-text').text('Silahkan isi jawaban terlebih dahulu!');
        const scrollPos = $('#error-text').offset().top - 170;
        $('html, body').animate({
            scrollTop: scrollPos
        }, 1500);
        return false;
    }
    if(!checkAnswerImage()){
        $('#error-text').text('Silahkan upload gambar terlebih dahulu!');
        const scrollPos = $('#error-text').offset().top - 170;
        $('html, body').animate({
            scrollTop: scrollPos
        }, 1500);
        return false;
    }
    fieldId++;
    let stringAppend = '';
    const type = $('#type').val();
    const answersType = $('#answers-type').val();
    if(type == 'multiple-choice') {
        if(answersType == 'image'){
            stringAppend = `
            <tr>
                <td width="25%">
                    <div class="custom-file mt-1">
                        <input type="file" name="answers_media_file[]" onchange="loadPreviewAnswer(this,`+""+fieldId+""+`)" id="ans_media_url`+""+fieldId+""+`" class="custom-file-input" value="">
                        <label class="custom-file-label" for="media">Choose file</label>
                    </div>
                    <input name="id_answers[]" value="new" type="hidden">
                </td>
                <td>
                    <div class="kt-avatar kt-avatar--outline kt-avatar--circle" style="width: auto;"><img class="kt-avatar__holder rounded" id="ans_media_preview`+""+fieldId+""+`" src="" onerror="this.src=`+"`"+urlNo+"`"+`" style="width: auto; height: 100px;"></div>
                    <input type="hidden" id="answers`+""+fieldId+""+`" name="answers[]" value="A`+""+fieldId+""+`">
                </td>
                <td width="5%">
                    <span class="kt-switch kt-switch--sm kt-switch--icon">
                    <label>
                        <input type="radio" name="true_answer" value="A`+""+fieldId+""+`">
                        <span></span>
                    </label>
                    </span>
                </td>
                <td width="5%">
                    <button type="button" title="Delete" class="btn btn-sm btn-icon btn-icon-md btn-label-danger delete-menu" onclick="removeSelf(this)"><i class="la la-trash"></i></button>
                </td>
            </tr>`;
        }else{
            stringAppend = '<tr>\n' +
            '<td>\n' +
            '<textarea class="form-control" placeholder="Jawaban" name="answers[]" onchange="setValue(this)"></textarea>\n' +
            '<input name="id_answers[]" value="new" type="hidden">\n' +
            '</td>\n' +
            '<td width="5%">\n' +
            '<span class="kt-switch kt-switch--sm kt-switch--icon">\n' +
            '<label>\n' +
            '<input type="radio" name="true_answer" value="" >\n' +
            '<span></span>\n' +
            '</label>\n' +
            '</span>\n' +
            '</td>\n' +
            '<td width="5%">\n' +
            '<button type="button" title="Delete" class="btn btn-sm btn-icon btn-icon-md btn-label-danger delete-menu" onclick="removeSelf(this)"><i class="la la-trash"></i></button>\n' +
            '</td>\n' +
            '</tr>';
        } 
    } else if(type == 'stacking-word') {
        stringAppend = '<tr>\n' +
            '<td>\n' +
            '<textarea class="form-control" placeholder="Jawaban" name="answers[]"></textarea>\n' +
            '<input name="id_answers[]" value="new" type="hidden">\n' +
            '</td>\n' +
            '<td width="5%">\n' +
            '<button type="button" title="Delete" class="btn btn-sm btn-icon btn-icon-md btn-label-danger delete-menu" onclick="removeSelf(this)"><i class="la la-trash"></i></button>\n' +
            '</td>\n' +
            '</tr>';
    }
    $('#list-answer').append(stringAppend);
    $('.btn-answers-media').filemanager('image', {prefix:domain});
    $('#ans_media_url'+""+fieldId+""+'').change(function(){
        $('#ans_media_preview'+""+fieldId+""+'').attr('src', $(this).val());
    });
});

$('#form-question').submit(function () {
    $('#error-text').text('');
    const type = $('#type').val();
    const answersType = $('#answers-type').val();
    if(answersType == 'image'){
        if(!checkAnswerImage() || !checkAnswerImageLength()) {
            $('#error-text').text('Silahkan isi jawaban terlebih dahulu!');
            const scrollPos = $('#error-text').offset().top - 170;
            $('html, body').animate({
                scrollTop: scrollPos
            }, 1500);
            return false;
        }
    }else{
        if(!checkAnswerText() || !checkAnswerLength()) {
            $('#error-text').text('Silahkan isi jawaban terlebih dahulu!');
            const scrollPos = $('#error-text').offset().top - 170;
            $('html, body').animate({
                scrollTop: scrollPos
            }, 1500);
            return false;
        }
    }
    if(type == 'stacking-word') {
        const answerWord = $('#answer-text');
        answerWord.removeClass('is-invalid');
        if(answerWord.val() == '') {
            answerWord.addClass('is-invalid');
            const scrollPos = $('#answer-text').offset().top - 170;
            $('html, body').animate({
                scrollTop: scrollPos
            }, 1500);
            return false;
        }
    } else {
        if(!checkSelectedAnswer()) {
            $('#error-text').text('Silahkan pilih jawaban benar terlebih dahulu!');
            const scrollPos = $('#error-text').offset().top - 170;
            $('html, body').animate({
                scrollTop: scrollPos
            }, 1500);
            return false;
        }
    }
});

function removeSelf(el) {
    $(el).parent().parent().remove();
}

function checkAnswerText() {
    let status = true;
    const choiceTextarea = $('#list-answer').find(":input[type='text'], textarea");
    for (var i = 0; i < choiceTextarea.length; i++)
    {
        if($(choiceTextarea[i]).val() == '')
        {
            status =  false;
        }
    }
    return status;
}

function checkAnswerImage(){
    let status = true;
    const choiceImage = $('#list-answer').find("img");
    for(var i = 0, length1 = choiceImage.length; i < length1; i++){
        if($(choiceImage[i]).attr('src') == '' || $(choiceImage).attr('src') == urlNo){
            status = false;
        }
    }
    return status;
}

function checkAnswerLength() {
    let status = true;
    const choiceTextarea = $('#list-answer').find(":input[type='text'], textarea");
    if(choiceTextarea.length == 0) {
        status = false;
    }
    return status;
}

function checkAnswerImageLength(){
    let status = true;
    const choiceTextarea = $('#list-answer').find("img");
    if(choiceTextarea.length == 0) {
        status = false;
    }
    return status;
}

function checkSelectedAnswer() {
    let status = true;
    const choiceRadio = $('#list-answer').find(":input[type='radio']:checked");
    if(choiceRadio.length == 0) {
        status = false;
    }
    return status;
}

function setValue(el){
    $(el).parent().parent().find(":input[type='radio']").val($(el).val());
}

function loadPreviewAnswer(input, id) {
    var id = '#ans_media_preview'+id;
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(id)
                    .attr('src', e.target.result)
                    .width('auto');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function removeMedia(id) {
    $('#'+id).remove();
}

function checkMediaFile() {
    let status = true;
    const questionFile = $('.media-question').find(".media-file");
    const questionPreview = $('.media-question').find(".media-preview");
    for(var i = 0, length1 = questionFile.length; i < length1; i++){
        if($(questionFile[i]).val() == '' && ($(questionPreview[i]).attr('src') == '' || $(questionPreview[i]).attr('src') == '/img/no-thumbnail.png')){
            status = false;
        }
    }
    return status;
}

function checkMaxField(max) {
    if (($('.media-question').find(".media-file")).length >= max) {
        $('#error-media').text('Batas media question adalah '+max+' file.');
        return false;
    } else {
        return true;
    }
}

function uploadMedia(id) {
    $('#'+id).click();
}

function previewMedia(input, id, type) {
    if (type == 'image') {
        var id = '#'+id || '#media_preview';
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(id).attr('src', e.target.result)
            };
            reader.readAsDataURL(input.files[0]);
        }
    } else if (type == 'audio') {
        if (input.files && input.files[0]) {
            var sound = document.getElementById(id);
                sound.src = URL.createObjectURL(input.files[0]);
                sound.onend = function(e) {
                    URL.revokeObjectURL(input.src);
                }
        }
    }
}