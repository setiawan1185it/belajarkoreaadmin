var session = OT.initSession(apiKey, sessionId);
var connection_count = 0;
session.on({
    connectionCreated: function (evt) {
        connection_count++;
        $('#jumlah_peserta').text(connection_count);
        getList();
    },
    connectionDestroyed: function (evt) {
        $('#user' + evt.connection.id).remove();
        connection_count--;
        $('#jumlah_peserta').text(connection_count);
        getList();
    }
});

session.on('sessionDisconnected', function sessionDisconnected(event) {
    alert('test');
    console.log('You were disconnected from the session.', event.reason);
});

// terima pesan
var msgHistory = $('#historiesMessage');
session.on('signal:msg', function signalCallback(event) {
    var detailPesan = JSON.parse(event.data);
    var userName = event.from.connectionId === session.connection.connectionId ? 'Anda' : detailPesan.name;
    var stringMesssage = '<div class="kt-chat__message kt-chat__message--right">\n' +
        '<div class="kt-chat__user">\n' +
        '<span class="kt-chat__datetime">' + detailPesan.time + '</span>\n' +
        '<a href="#" class="kt-chat__username">' + userName + '</a>\n' +
        '<span class="kt-media kt-media--circle kt-media--sm">\n' +
        '<img src="'+ detailPesan.photo +'" alt="image">\n' +
        '</span>\n' +
        '</div>\n' +
        '<div class="kt-chat__text kt-bg-light-brand">\n' +
        detailPesan.pesan +
        '</div>\n' +
        '</div>';
    var stringMessageTheir = '<div class="kt-chat__message">\n' +
        '<div class="kt-chat__user">\n' +
        '<span class="kt-media kt-media--circle kt-media--sm">\n' +
        '<img src="'+ detailPesan.photo +'" alt="image">\n' +
        '</span>\n' +
        '<a href="#" class="kt-chat__username">' + userName + '</a>\n' +
        '<span class="kt-chat__datetime">' + detailPesan.time + '</span>\n' +
        '</div>\n' +
        '<div class="kt-chat__text kt-bg-light-success">\n' +
        detailPesan.pesan +
        '</div>\n' +
        '</div>';
    msgHistory.append(event.from.connectionId === session.connection.connectionId ? stringMesssage : stringMessageTheir);
    msgHistory.parent().animate({
        scrollTop: msgHistory.parent()[0].scrollHeight
    }, 300);
});


var jsonData = {
    name: name,
    role: role,
    photo: photo,
    id: id,
}

var publisherOptions = {
    insertMode: 'append',
    width: '100%',
    height: '100%',
    name: JSON.stringify(jsonData),
    style: { nameDisplayMode: "off" },
    publishAudio: true,
    publishVideo: true,
    resolution: '1280x720',
    frameRate: 30
};

var idPublisher = 'subscriber';
if(role == 'teacher') {
    idPublisher = 'subscriber_teacher';
} else {
    if(streamingStatus <= 4) {
        idPublisher = 'subscriber';
    } else {
        idPublisher = 'subscriber_hide';
    }
}

var publisher = OT.initPublisher(idPublisher, publisherOptions, function (error) {
    if(error) {
        console.log('maaf terjadi kesalahan saat publish streaming', error);
    } else {
        $('#' + idPublisher).children().last().attr('data-id', id);
        if(role == 'student') {
            $('#' + idPublisher).children().addClass('panel-streaming');
        }
    }
});

session.connect(token, async function callback(error) {
    if (error) {
        console.log('error');
    } else {
        var detail = await getDetail(streamingId, id);
        if(detail.error == 'false') {
            var dataDetail = detail.data;
            console.log(dataDetail);
            if(dataDetail.active == 1 && dataDetail.status <= 4) {
                session.publish(publisher);
            } else {
                publisher.publishAudio(false);
                publisher.publishVideo(false);
                session.publish(publisher);
                postUpdate(streamingId, id, 99, 1);
            }
        } else {
            alert('Maaf terjadi kesalahan silahkan refresh halaman anda!');
        }
        reloadLayar();
        getList();

        // kirim pesan
        var form = $('#formMessage');
        var msgTxt = $('#textMessage');
        form.submit( function submit(event) {
            event.preventDefault();
            if(msgTxt.val() == '') {
                return false;
            }
            jsonData.pesan = msgTxt.val();
            var now = new Date();
            jsonData.time = now.getHours() + ':' + now.getMinutes();
            session.signal({
                type: 'msg',
                data: JSON.stringify(jsonData),
            }, function signalCallback(error) {
                if (error) {
                    console.error('Error sending signal:', error.name, error.message);
                } else {
                    msgTxt.val('');
                }
            });
        });

        msgTxt.keypress(function (e) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                if(msgTxt.val() == '') {
                    return false;
                }
                form.submit();
            }
        });

        $("#peserta").on("click", '.kt-widget__item .kt-widget__action .dropdown-menu .dropdown-item', function (e) {
            var jsonClick = {
                layar: $(this).data('layar'),
                idUser: $(this).parent().data('id')
            };
            session.signal({
                type: 'klik',
                data: JSON.stringify(jsonClick),
            }, function signalCallback(error) {
                if (error) {
                    console.error('Error sending signal:', error.name, error.message);
                } else {
                    reloadLayar();
                    console.log('kuatkan hamba');
                }
            });
        });


        session.on('signal:klik', async function signalCallback(event) {
            var klikData = JSON.parse(event.data);
            if(klikData.idUser == parseInt(id)) {
                if(klikData.layar == 1) {
                    await postUpdate(streamingId, id, 2, 1);
                    publisher.publishAudio(true);
                    publisher.publishVideo(true);
                } else {
                    await postUpdate(streamingId, id, 99, 1);
                    publisher.publishAudio(false);
                    publisher.publishVideo(false);
                }
                session.signal({
                    type: 'klikselesai',
                    data: (klikData.layar).toString(),
                }, function signalCallback(error) {
                    if (error) {
                        console.error('Error sending signal:', error.name, error.message);
                    } else {
                        reloadLayar();
                        console.log('kuatkan hamba, klikselesai');
                    }
                });
            }
        });

        session.on('signal:klikselesai', async function signalCallback(event) {
            var klikData = parseInt(event.data);
            if(klikData == 1) {
                reloadHide();
            } else {
                reloadMain();
            }
            getList();
        });
    }
});

session.on('streamCreated', async function streamCreated(event) {
    var eventData = JSON.parse(event.stream.name);
    var subscriberOptions = {
        insertMode: 'append',
        width: '100%',
        height: '100%',
        name: eventData.name,
        role: eventData.role,
        photo: eventData.photo,
    };
    var detail = await getDetail(streamingId, eventData.id);
    if(detail.data.status == 1) {
        await subscribe(session, subscriberOptions, eventData, event, 'subscriber_teacher');
    } else if(detail.data.status <= 4 && detail.data.status > 1) {
        await subscribe(session, subscriberOptions, eventData, event, 'subscriber');
    } else {
        await subscribe(session, subscriberOptions, eventData, event, 'subscriber_hide');
    }
});

async function subscribe(session, subscriberOptions, eventData, event, subscriberId) {
    session.subscribe(event.stream, subscriberId, subscriberOptions, function (error) {
        if (!event.stream.hasAudio) {
            console.log('tidak ada audio = ' + eventData.name);
        }
        if (!event.stream.hasVideo) {
            console.log('tidak ada video = ' + eventData.name);
        }
        if(error) {
            console.log('Maaf terjadi kesalahan, mohon refresh halaman ini lagi!');
        } else {
            $('#' + subscriberId).children().last().attr('data-id', eventData.id);
            if(eventData.role != 'teacher') {
                $('#' + subscriberId).children().addClass('panel-streaming');
            }
            getList();
            reloadLayar();
        }
    });
}

// postUpdate(streamingId, id, 99, 0);

async function postUpdate(id_streaming, id_user, status, active) {
    $.post( URL_POST, { id_streaming: streamingId, id_user: id_user, status: status, active: active }, function(response) {
        console.log('post', response);
        if(status > 4) {
            reloadMain();
        } else {
            reloadHide();
        }
        getList();
        reloadLayar();
    }).fail(function(error) {
        console.log(error);
    });
}

// getList();

function getList() {
    $.get( URL_LIST, { code: streamingCode }, function(response) {
        var list = response.data;
        $('#peserta').empty();
        for(var i = 0; i < list.length; i++) {
            var textAdditionalHtml = '<a class="dropdown-item cursor" data-layar="1"><i class="la la-plus"></i> Tambahkan</a>\n';
            if ((list[i].status <= 4) ) {
                textAdditionalHtml = '<a class="dropdown-item cursor"  data-layar="0"><i class="la la-minus"></i> Hilangkan</a>\n';
            }
            var additionalHtml = '<button type="button" class="btn btn-brand btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
                '<i class="flaticon-more-1"></i>\n' +
                '</button>\n' +
                '<div class="dropdown-menu dropdown-menu-right" data-id="'+ list[i].id_user +'">\n' +
                 textAdditionalHtml +
                '</div>';
            var activeClass = (list[i].status <= 4) ? 'kt-badge kt-badge--success kt-badge--dot' : 'kt-badge kt-badge--danger kt-badge--dot';
            if(role == 'student') {
                additionalHtml = '';
            }
            if(list[i].role.name == 'teacher') {
                additionalHtml = '';
            }
            var photo_url = 'https://aproject-dev.eduinkor.com/img/blank-avatar.png';
            if(list[i].user.photo_url) {
                photo_url = list[i].user.photo_url;
            }
            var stringPendaftar = '<div class="kt-widget__item" id="user'+ list[i].id_user +'" data-id="'+ list[i].id_user +'">\n' +
                '<span class="kt-media kt-media--circle">\n' +
                '<img src="' + photo_url + '" alt="image" onerror="this.src=\'https://aproject-dev.eduinkor.com/img/blank-avatar.png\'">\n' +
                '</span>\n' +
                '<div class="kt-widget__info">\n' +
                '<div class="kt-widget__section">\n' +
                '<a href="#" class="kt-widget__username">'+ list[i].user.name +'</a>\n' +
                '<span class="' + activeClass + '"></span>\n' +
                '</div>\n' +
                '<span class="kt-widget__desc">\n' +
                list[i].role.name + '\n' +
                '</span>\n' +
                '</div>\n' +
                '<div class="kt-widget__action">\n' +
                additionalHtml +
                '</div>\n' +
                '</div>';
            $('#peserta').append(stringPendaftar);
        }
    }).fail(function(error) {
        console.log(error);
    });
}


async function getDetail(id_streaming, id_user) {
    var result = await $.get( URL_DETAIL, { id_streaming: id_streaming, id_user: id_user });
    return result;
}

function reloadMain() {
    $.get( URL_LIST_MAIN, { code: streamingCode }, function(response) {
        var arrayId = response.data;
        console.log('reloadMain', response.data);
        var jumlahLayar = $('#subscriber').children('div').length;
        $('#subscriber').children('div').each(function (index, value) {
            if(index == 4) {
                // if(jumlahLayar == 5) {
                //     $(value).removeClass('panel-streaming');
                //     $(value).removeClass('full-streaming');
                //     $(value).addClass('full-streaming');
                // } else if(jumlahLayar > 5) {
                //     $(value).removeClass('full-streaming');
                //     $(value).removeClass('panel-streaming');
                //     $(value).addClass('panel-streaming');
                // }
                // var element = $(value).children();
                // var dataId = $(element).attr('data-id');

            } else if(index > 4) {
                var element = $(value);
                var dataId = parseInt($(element).attr('data-id'));
                if(dataId != 'undefined' && !inArray(arrayId, dataId)) {
                    $(element).appendTo('#subscriber_hide');
                } else {
                    console.log('ada atau undefined');
                }
            }
        });
        reloadLayar();
    });
}


function reloadHide() {
    $.get( URL_LIST_HIDE, { code: streamingCode }, function(response) {
        var arrayId = response.data;
        console.log('reloadHide', response.data);
        $('#subscriber_hide').children('div').each(function (index, value) {
            var element = $(value);
            var dataId = parseInt($(element).attr('data-id'));
            if(dataId != 'undefined' && !inArray(arrayId, dataId)) {
                console.log(dataId, arrayId, dataId != 'undefined' && !inArray(arrayId, dataId));
                $(element).appendTo('#subscriber');
            } else {
                console.log('ada atau undefined');
            }
        });
        reloadLayar();
    });
}

function reloadLayar() {
    var jumlahLayar = $('#subscriber').children('div').length;
    $('#subscriber').children('div').each(function (index, value) {
        if(index == 4) {
            if(jumlahLayar == 5) {
                $(value).removeClass('panel-streaming');
                $(value).removeClass('full-streaming');
                $(value).addClass('full-streaming');
            } else if(jumlahLayar > 5) {
                $(value).removeClass('full-streaming');
                $(value).removeClass('panel-streaming');
                $(value).addClass('panel-streaming');
            }
        }
    });
}

$('#subscriber').dblclick(function (e) {
    var elem = document.getElementById('subscriber');
    openFullscreen();
    function openFullscreen() {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) { /* Firefox */
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { /* IE/Edge */
            elem.msRequestFullscreen();
        }
    }
});