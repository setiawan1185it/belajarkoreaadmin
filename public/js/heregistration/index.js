jQuery(document).ready(function() {
    var formEl = $('#heregistrion');
    var initValidation = function() {
        var validator = formEl.validate({
            ignore: ":hidden",
            focusInvalid: false,
            invalidHandler: function(form, validator) {

                if (!validator.numberOfInvalids())
                    return;

                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 50
                }, 2000);
            },
            submitHandler: function (form) {
                form.submit();
                Swal.fire({
                    title: 'Mohon tunggu sebentar!',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    onOpen: () => {
                        swal.showLoading();
                    }
                });
            }
        });
    }
    initValidation();
});