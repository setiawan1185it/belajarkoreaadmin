var connection_count = 0;
var publisher_count = 1;
var countSubscriber = 1;

function handleError(error) {
    if (error) {
        console.error(error);
    }
}

initializeSession();

function initializeSession() {
    var session = OT.initSession(apiKey, sessionId);
    var today = new Date();
    session.on({
        connectionCreated: function (evt) {
            connection_count++;
            $('#jumlah_peserta').text(connection_count);
        },
        connectionDestroyed: function (evt) {
            $('#user' + evt.connection.id).remove();
            connection_count--;
            $('#jumlah_peserta').text(connection_count);
        }
    });

    session.on('streamCreated', function streamCreated(event) {
        var eventData = JSON.parse(event.stream.name);
        var subscriberOptions = {
            insertMode: 'append',
            width: '100%',
            height: '100%',
            name: eventData.name,
            role: eventData.role,
            photo: eventData.photo,
            time: eventData.time,
        };
        if(role == 'teacher' && eventData.active) {
            // if(countSubscriber == 1) {
            //     subscribe(session, subscriberOptions, eventData, event, 'subscriber_teacher');
            // } else if (countSubscriber <= 3){
            //     subscribe(session, subscriberOptions, eventData, event, 'subscriber' + (countSubscriber - 1));
            // } else {
            //     subscribe(session, subscriberOptions, eventData, event, 'subscriber_hide');
            // }
            // countSubscriber++;
            subscribe(session, subscriberOptions, eventData, event, 'subscriber');
        } else if(role == 'student' && eventData.active) {
            // if(eventData.role == 'teacher') {
            //     subscribe(session, subscriberOptions, eventData, event, 'subscriber_teacher');
            // } else if(eventData.role == 'student' && countSubscriber <= 3) {
            //     subscribe(session, subscriberOptions, eventData, event, 'subscriber' + countSubscriber);
            //     countSubscriber++;
            // } else {
            //     subscribe(session, subscriberOptions, eventData, event, 'subscriber_hide');
            //     countSubscriber++;
            // }
            if(eventData.role == 'teacher') {
                subscribe(session, subscriberOptions, eventData, event, 'subscriber_teacher');
            } else {
                subscribe(session, subscriberOptions, eventData, event, 'subscriber');
            }
        } else {
            // subscribe(session, subscriberOptions, eventData, event, 'subscriber_hide');
            subscribe(session, subscriberOptions, eventData, event, 'subscriber');
        }
    });

    function subscribe(session, subscriberOptions, eventData, event, subscriberId) {
        session.subscribe(event.stream, subscriberId, subscriberOptions, function (error) {
            if (!event.stream.hasAudio) {
                console.log('tidak ada audio = ' + eventData.name);
            }
            if (!event.stream.hasVideo) {
                console.log('tidak ada video = ' + eventData.name);
            }
            if(error) {
            } else {
                publisher_count++;
                let stringPendaftar = '<div class="kt-widget__item" id="user'+ event.stream.connection.id+'" data-id="' + eventData.id + '">\n' +
                    '<span class="kt-media kt-media--circle">\n' +
                    '<img src="' + eventData.photo + '" alt="image">\n' +
                    '</span>\n' +
                    '<div class="kt-widget__info">\n' +
                    '<div class="kt-widget__section">\n' +
                    '<a href="#" class="kt-widget__username">'+ eventData.name +'</a>\n' +
                    '<span class="kt-badge kt-badge--success kt-badge--dot"></span>\n' +
                    '</div>\n' +
                    '<span class="kt-widget__desc">\n' +
                    eventData.role + '\n' +
                    '</span>\n' +
                    '</div>\n' +
                    '<div class="kt-widget__action">\n' +
                    '<button type="button" class="btn btn-brand btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
                    '<i class="flaticon-more-1"></i>\n' +
                    '</button>\n' +
                    '<div class="dropdown-menu dropdown-menu-right" data-id="'+ eventData.id +'">\n' +
                    '<a class="dropdown-item" href="#" data-layar="1"><i class="la la-plus"></i> Layar 1</a>\n' +
                    '<a class="dropdown-item" href="#" data-layar="2"><i class="la la-plus"></i> Layar 2</a>\n' +
                    '<a class="dropdown-item" href="#" data-layar="3"><i class="la la-plus"></i> Layar 3</a>\n' +
                    '<a class="dropdown-item" href="#" data-layar="4"><i class="la la-plus"></i> Layar 4</a>\n' +
                    '</div>' +
                    '</div>\n' +
                    '</div>';
                $('#peserta').append(stringPendaftar);
            }
        });
    }


    session.on('sessionDisconnected', function sessionDisconnected(event) {
        alert('test');
        console.log('You were disconnected from the session.', event.reason);
    });


    // terima pesan
    var msgHistory = $('#historiesMessage');
    session.on('signal:msg', function signalCallback(event) {
        var detailPesan = JSON.parse(event.data);
        var userName = event.from.connectionId === session.connection.connectionId ? 'Anda' : detailPesan.name;
        var stringMesssage = '<div class="kt-chat__message kt-chat__message--right">\n' +
            '<div class="kt-chat__user">\n' +
            '<span class="kt-chat__datetime">' + detailPesan.time + '</span>\n' +
            '<a href="#" class="kt-chat__username">' + userName + '</a>\n' +
            '<span class="kt-media kt-media--circle kt-media--sm">\n' +
            '<img src="'+ detailPesan.photo +'" alt="image">\n' +
            '</span>\n' +
            '</div>\n' +
            '<div class="kt-chat__text kt-bg-light-brand">\n' +
            detailPesan.pesan +
            '</div>\n' +
            '</div>';
        var stringMessageTheir = '<div class="kt-chat__message">\n' +
            '<div class="kt-chat__user">\n' +
            '<span class="kt-media kt-media--circle kt-media--sm">\n' +
            '<img src="'+ detailPesan.photo +'" alt="image">\n' +
            '</span>\n' +
            '<a href="#" class="kt-chat__username">' + userName + '</a>\n' +
            '<span class="kt-chat__datetime">' + detailPesan.time + '</span>\n' +
            '</div>\n' +
            '<div class="kt-chat__text kt-bg-light-success">\n' +
            detailPesan.pesan +
            '</div>\n' +
            '</div>';
        msgHistory.append(event.from.connectionId === session.connection.connectionId ? stringMesssage : stringMessageTheir);
        msgHistory.parent().animate({
            scrollTop: msgHistory.parent()[0].scrollHeight
        }, 300);
    });


    var jsonData = {
        name: name,
        role: role,
        photo: photo,
        time: today.getHours() + ' : ' + today.getMinutes(),
        id: id,
        count_active: connection_count,
    }

    var active = false;
    if(role == 'teacher') {
        active = true;
    } else if(role == 'student' && connection_count <= 3) {
        active = true;
    }

    jsonData.active = active;
    var publisherOptions = {
        insertMode: 'append',
        width: '100%',
        height: '100%',
        name: JSON.stringify(jsonData),
        style: { nameDisplayMode: "off" },
        publishAudio: active,
        publishVideo: active
    };

    var idPublisher = 'subscriber';
    if(role == 'teacher') {
        idPublisher = 'subscriber_teacher';
    }

    var publisher = OT.initPublisher(idPublisher, publisherOptions, function (error) {
        if(error) {
        } else {
            var classActive = (active) ? 'kt-badge--success' : 'kt-badge--danger';
            var stringPendaftar = '<div class="kt-widget__item" id="user'+ id +'" data-id="'+ id +'">\n' +
            '<span class="kt-media kt-media--circle">\n' +
            '<img src="' + photo + '" alt="image">\n' +
            '</span>\n' +
            '<div class="kt-widget__info">\n' +
            '<div class="kt-widget__section">\n' +
            '<a href="#" class="kt-widget__username">'+ name +'</a>\n' +
            '<span class="kt-badge kt-badge--success kt-badge--dot"></span>\n' +
            '</div>\n' +
            '<span class="kt-widget__desc">\n' +
             role + '\n' +
            '</span>\n' +
            '</div>\n' +
            '<div class="kt-widget__action">\n' +
            '<button type="button" class="btn btn-brand btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
            '<i class="flaticon-more-1"></i>\n' +
            '</button>\n' +
            '<div class="dropdown-menu dropdown-menu-right" data-id="'+ id +'">\n' +
            '<a class="dropdown-item" href="#" data-layar="1"><i class="la la-plus"></i> Layar 1</a>\n' +
            '<a class="dropdown-item" href="#" data-layar="2"><i class="la la-plus"></i> Layar 2</a>\n' +
            '<a class="dropdown-item" href="#" data-layar="3"><i class="la la-plus"></i> Layar 3</a>\n' +
            '<a class="dropdown-item" href="#" data-layar="4"><i class="la la-plus"></i> Layar 4</a>\n' +
            '</div>' +
            '</div>\n' +
            '</div>';
            $('#peserta').append(stringPendaftar);
        }
    });

    session.connect(token, function callback(error) {
        if (error) {
            handleError(error);
        } else {
            session.publish(publisher, handleError);

            // kirim pesan
            var form = $('#formMessage');
            var msgTxt = $('#textMessage');
            form.submit( function submit(event) {
                event.preventDefault();
                if(msgTxt.val() == '') {
                    return false;
                }
                jsonData.pesan = msgTxt.val();
                var now = new Date();
                jsonData.time = now.getHours() + ':' + now.getMinutes();
                session.signal({
                    type: 'msg',
                    data: JSON.stringify(jsonData),
                }, function signalCallback(error) {
                    if (error) {
                        console.error('Error sending signal:', error.name, error.message);
                    } else {
                        msgTxt.val('');
                    }
                });
            });

            msgTxt.keypress(function (e) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    if(msgTxt.val() == '') {
                        return false;
                    }
                    form.submit();
                }
            });

            $("#peserta").on("click", '.kt-widget__item .kt-widget__action .dropdown-menu .dropdown-item', function (e) {
                var jsonClick = {
                    layar: $(this).data('layar'),
                    idUser: $(this).parent().data('id')
                };
                session.signal({
                    type: 'klik',
                    data: JSON.stringify(jsonClick),
                }, function signalCallback(error) {
                    if (error) {
                        console.error('Error sending signal:', error.name, error.message);
                    } else {
                        console.log('kuatkan hamba');
                    }
                });
            });


            session.on('signal:klik', function signalCallback(event) {
                var klikData = JSON.parse(event.data);
                if(klikData.idUser == parseInt(id)) {
                    // publisher.publishAudio(false);
                    // publisher.publishVideo(false);
                    session.publish(publisher);
                }
            });
        }
    });

    // postUpdate(streamingId, id, 99, 0);

    function postUpdate(id_streaming, id_user, status, active) {
        $.post( URL_POST, { id_streaming: streamingId, id_user: id_user, status: status, active: active }, function(response) {
            console.log('post', response);
        }).fail(function(error) {
            console.log(error);
        });
    }

    // getList();

    function getList() {
        $.get( URL_LIST, { code: streamingCode }, function(response) {
            console.log(response);
        }).fail(function(error) {
            console.log(error);
        });
    }
}