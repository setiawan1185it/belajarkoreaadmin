$('#media_url').change(function () {
    if($('#media-type').val()== 'image'){
        $('#media_preview').attr('src', $(this).val());
    }else{
        $('#media_preview_audio').attr('src', $(this).val());
    }
});

const mediatype = $('#media-type').val();
const elMedia = $('.media');
const elMediaImage = $('.media-image');
const elMediaAudio = $('.media-audio');

elMediaImage.hide();
elMediaAudio.hide();
elMedia.hide();

if(mediatype == 'image') {
    elMedia.show();
    elMediaImage.show();
    $('#lfm').filemanager(mediatype, {prefix: domain});
} else if(mediatype == 'audio') {
    elMedia.show();
    elMediaAudio.show();
    $('#lfm').filemanager(mediatype, {prefix: domain});
} 

$('#media-type').change(function () {
    elMedia.hide();
    elMediaImage.hide();
    elMediaAudio.hide();
    const mediatype = $(this).val();
    if(mediatype == 'image') {
        elMedia.show();
        elMediaImage.show();
        $('#lfm').filemanager(mediatype, {prefix: domain});
        if(elMediaImage.find('img').attr('src')!= "" && elMediaImage.find('img').attr('src') != urlNo){
            $('#media_url').val(elMediaImage.find('img').attr('src'));
        }else{
            $('#media_url').val('');
        }
    }else if(mediatype == 'audio') {
        elMedia.show();
        elMediaAudio.show();
        $('#lfm').filemanager(mediatype, {prefix: domain});
        if($('#media_preview_audio').attr('src')!=""){
            $('#media_url').val(elMediaAudio.find('audio').attr('src'));
        }else{
            $('#media_url').val('');
        }
    }else{
        $('#media_url').val('');
    }
});

$('#removeImage').click(function () {
    $('#media_url').val('');
    elMediaImage.hide();
});

$('#removeAudio').click(function () {
    $('#media_url').val('');
    elMediaAudio.hide();
});

const type = $('#type').val();
$('#type').change(function () {
    if($(this).val() == 'stacking-word') {
        $('#word-answer').show();
    } else {
        $('#word-answer').hide();
    }
});



$('#answers-type').change(function(){
    $('#list-answer').find('tr').remove();
    fieldId = 0;
})


$('#add-answer').click(function () {
    $('#error-text').text('');
    if(!checkAnswerText()) {
        $('#error-text').text('Silahkan isi jawaban terlebih dahulu!');
        const scrollPos = $('#error-text').offset().top - 170;
        $('html, body').animate({
            scrollTop: scrollPos
        }, 1500);
        return false;
    }
    if(!checkAnswerImage()){
        $('#error-text').text('Silahkan upload gambar terlebih dahulu!');
        const scrollPos = $('#error-text').offset().top - 170;
        $('html, body').animate({
            scrollTop: scrollPos
        }, 1500);
        return false;
    }
    fieldId++;
    let stringAppend = '';
    const type = $('#type').val();
    const answersType = $('#answers-type').val();
    if(type == 'multiple-choice') {
        if(answersType == 'image'){
            stringAppend = '<tr>\n' +
            '<td>\n' +
            '<button type="button" data-input="ans_media_url'+""+fieldId+""+'" data-preview="ans_media_preview'+""+fieldId+""+'" class="btn btn-outline-brand btn-elevate btn-icon mb-3 btn-answers-media"><i class="la la-cloud-upload"></i></button>\n' +
            '<input name="id_answers[]" value="new" type="hidden">\n' +
            '</td>\n' +
            '<td>\n'+
            '<input type="hidden" name="answers_media_url[]" id="ans_media_url'+""+fieldId+""+'" value="">\n'+
            '<div class="kt-avatar kt-avatar--outline kt-avatar--circle" style="width: auto"><img class="kt-avatar__holder rounded" id="ans_media_preview'+""+fieldId+""+'" src="" onerror="this.src='+"'"+urlNo+"'"+'" style="width: auto; height: 100px;"></div>\n'+
            '<input type="hidden" id="answers'+""+fieldId+""+'" name="answers[]" value="A'+""+fieldId+""+'">\n'+
            '</td>\n'+
            '<td width="5%">\n' +
            '<span class="kt-switch kt-switch--sm kt-switch--icon">\n' +
            '<label>\n' +
            '<input type="radio" name="true_answer" value="A'+""+fieldId+""+'">\n' +
            '<span></span>\n' +
            '</label>\n' +
            '</span>\n' +
            '</td>\n' +
            '<td width="5%">\n' +
            '<button type="button" title="Delete" class="btn btn-sm btn-icon btn-icon-md btn-label-danger delete-menu" onclick="removeSelf(this)"><i class="la la-trash"></i></button>\n' +
            '</td>\n' +
            '</tr>';
        }else{
            stringAppend = '<tr>\n' +
            '<td>\n' +
            '<textarea class="form-control" placeholder="Jawaban" name="answers[]" onchange="setValue(this)"></textarea>\n' +
            '<input name="id_answers[]" value="new" type="hidden">\n' +
            '</td>\n' +
            '<td width="5%">\n' +
            '<span class="kt-switch kt-switch--sm kt-switch--icon">\n' +
            '<label>\n' +
            '<input type="radio" name="true_answer" value="" >\n' +
            '<span></span>\n' +
            '</label>\n' +
            '</span>\n' +
            '</td>\n' +
            '<td width="5%">\n' +
            '<button type="button" title="Delete" class="btn btn-sm btn-icon btn-icon-md btn-label-danger delete-menu" onclick="removeSelf(this)"><i class="la la-trash"></i></button>\n' +
            '</td>\n' +
            '</tr>';
        } 
    } else if(type == 'stacking-word') {
        stringAppend = '<tr>\n' +
            '<td>\n' +
            '<textarea class="form-control" placeholder="Jawaban" name="answers[]"></textarea>\n' +
            '<input name="id_answers[]" value="new" type="hidden">\n' +
            '</td>\n' +
            '<td width="5%">\n' +
            '<button type="button" title="Delete" class="btn btn-sm btn-icon btn-icon-md btn-label-danger delete-menu" onclick="removeSelf(this)"><i class="la la-trash"></i></button>\n' +
            '</td>\n' +
            '</tr>';
    }
    $('#list-answer').append(stringAppend);
    $('.btn-answers-media').filemanager('image', {prefix:domain});
    $('#ans_media_url'+""+fieldId+""+'').change(function(){
        $('#ans_media_preview'+""+fieldId+""+'').attr('src', $(this).val());
    });
});

$('#form-question').submit(function () {
    $('#error-text').text('');
    const type = $('#type').val();
    const answersType = $('#answers-type').val();
    if(answersType == 'image'){
        if(!checkAnswerImage() || !checkAnswerImageLength()) {
            $('#error-text').text('Silahkan isi jawaban terlebih dahulu!');
            const scrollPos = $('#error-text').offset().top - 170;
            $('html, body').animate({
                scrollTop: scrollPos
            }, 1500);
            return false;
        }
    }else{
        if(!checkAnswerText() || !checkAnswerLength()) {
            $('#error-text').text('Silahkan isi jawaban terlebih dahulu!');
            const scrollPos = $('#error-text').offset().top - 170;
            $('html, body').animate({
                scrollTop: scrollPos
            }, 1500);
            return false;
        }
    }
    if(type == 'stacking-word') {
        const answerWord = $('#answer-text');
        answerWord.removeClass('is-invalid');
        if(answerWord.val() == '') {
            answerWord.addClass('is-invalid');
            const scrollPos = $('#answer-text').offset().top - 170;
            $('html, body').animate({
                scrollTop: scrollPos
            }, 1500);
            return false;
        }
    } else {
        if(!checkSelectedAnswer()) {
            $('#error-text').text('Silahkan pilih jawaban benar terlebih dahulu!');
            const scrollPos = $('#error-text').offset().top - 170;
            $('html, body').animate({
                scrollTop: scrollPos
            }, 1500);
            return false;
        }
    }
});

function removeSelf(el) {
    $(el).parent().parent().remove();
}

function checkAnswerText() {
    let status = true;
    const choiceTextarea = $('#list-answer').find(":input[type='text'], textarea");
    for (var i = 0; i < choiceTextarea.length; i++)
    {
        if($(choiceTextarea[i]).val() == '')
        {
            status =  false;
        }
    }
    return status;
}

function checkAnswerImage(){
    let status = true;
    const choiceImage = $('#list-answer').find("img");
    for(var i = 0, length1 = choiceImage.length; i < length1; i++){
        if($(choiceImage[i]).attr('src') == '' || $(choiceImage).attr('src') == urlNo){
            status = false;
        }
    }
    return status;
}

function checkAnswerLength() {
    let status = true;
    const choiceTextarea = $('#list-answer').find(":input[type='text'], textarea");
    if(choiceTextarea.length == 0) {
        status = false;
    }
    return status;
}

function checkAnswerImageLength(){
    let status = true;
    const choiceTextarea = $('#list-answer').find("img");
    if(choiceTextarea.length == 0) {
        status = false;
    }
    return status;
}

function checkSelectedAnswer() {
    let status = true;
    const choiceRadio = $('#list-answer').find(":input[type='radio']:checked");
    if(choiceRadio.length == 0) {
        status = false;
    }
    return status;
}

function setValue(el){
    $(el).parent().parent().find(":input[type='radio']").val($(el).val());
}
