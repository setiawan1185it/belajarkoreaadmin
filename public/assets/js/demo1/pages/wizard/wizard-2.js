"use strict";

// Class definition
var KTWizard2 = function () {
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;
    
    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v2', {
            startStep: 1,
        });

        // Validation before going to next page
        wizard.on('beforeNext', function(wizardObj) {
            if (validator.form() !== true) {
                wizardObj.stop();  // don't go to the next step
            }
        })

        // Change event
        wizard.on('change', function(wizard) {
            KTUtil.scrollTop();    
        });
    }

    var initValidation = function() {
        $.validator.addMethod(
            "maxfilesize",
            function (value, element) {
                if (this.optional(element) || ! element.files || ! element.files[0]) {
                    return true;
                } else {
                    return element.files[0].size <= 1024 * 1024 * 5;
                }
            },
            'The file size can not exceed 5MB.'
        );
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
               	//= Step 1
				name: {
					required: true
				},
				nickname: {
					required: true
				},
                born_place: {
				    required: true
                },
                born_date: {
				    required: true
                },
                gender: {
				    required:true
                },
                email: {
				    required: true,
                    email: true
                },
                temporary_address: {
					required: true,
				},
                temporary_region: {
                    required: true,
                },
                temporary_city: {
                    required: true,
                },
                temporary_phone: {
					required: true,
                    number: true
				},
                permanent_region: {
                    required: true,
                },
                permanent_city: {
                    required: true,
                },
                permanent_address: {
                    required: true
                },
                permanent_phone: {
                    required: true,
                    number: true
                },

				//= Step 2
                sd_name: {
					required: true
				},
                sd_masuk: {
					required: true
				},
                sd_lulus: {
					required: true
				},

                smp_name: {
                    required: true
                },
                smp_masuk: {
                    required: true
                },
                smp_lulus: {
                    required: true
                },

                sma_name: {
                    required: true
                },
                sma_masuk: {
                    required: true
                },
                sma_lulus: {
                    required: true
                },

                'keterampilan[]': {
				    required: true
                },
                'tingkat[]': {
				    required: true
                },
                'keterangan[]': {
				    required: true
                },

                'kursus[]': {
				    required: true
                },
                'tahun[]': {
				    required: true
                },
                'nilai[]': {
				    required: true
                },
                'penyelenggara[]': {
				    required: true
                },

				//= Step 4
				question1: {
					required: true
				},
                question2: {
                    required: true
                },
                question3: {
                    required: true
                },


				//= Step 5
                ktp: {
                    required: true,
                    extension: "jpg,jpeg,png",
                    maxfilesize: true,
				},
                ijazah: {
                    required: true,
                    extension: "jpg,jpeg,png",
                    maxfilesize: true,
                },
                // transkip: {
                //     required: true,
                //     extension: "jpg,jpeg,png",
                //     maxfilesize: true,
                // },
            },
            focusInvalid: false,
            
            // Display error  
            // invalidHandler: function(event, validator) {
            //     KTUtil.scrollTop();
            //
            //     swal.fire({
            //         "title": "",
            //         "text": "There are some errors in your submission. Please correct them.",
            //         "type": "error",
            //     });
            // },
            invalidHandler: function(form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    var firstInvalidElement = $(validator.errorList[0].element);
                    // $('html,body').scrollTop(firstInvalidElement.offset().top);
                    $('html, body').animate({ scrollTop: firstInvalidElement.offset().top}, 1000);
                    //firstInvalidElement.focus();
                }
            },

            // Submit valid form
            submitHandler: function (form) {
                form.submit();
            }
        });   
    }


    return {
        // public functions
        init: function() {
            wizardEl = KTUtil.get('kt_wizard_v2');
            formEl = $('#kt_form');

            initWizard(); 
            initValidation();
        }
    };
}();

jQuery(document).ready(function() {    
    KTWizard2.init();
});