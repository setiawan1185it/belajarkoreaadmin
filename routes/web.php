<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'admin@index');
Route::post('daftar','admin@daftaradmin');
Route::post('loginmasukadmin','admin@loginmasuk');
Route::get('contents','admin@contents');
Route::get('show/{id}','admin@showdatakonten');
Route::get('edit/{id}','admin@editdatakonten');
Route::post('simpandata','admin@simpandatakonten');
Route::get('create','admin@createdata');
Route::post('pencarian','admin@caridata');
Route::post('simpandatabaru','admin@simpandatabarukonten');
Route::post('logout','admin@keluar');
Route::get('quiz','admin@quiznew');
Route::get('editquiz/{id}','admin@editquiz');

