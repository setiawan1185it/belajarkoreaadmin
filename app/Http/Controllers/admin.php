<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\ModelUser2;
class admin extends Controller
{
    public function index(){
    	return view('welcome');
    }
    public function daftaradmin(Request $request){
    	$fullname=$request->input('fullname');
    	$email=$request->input('email');
    	$password= bcrypt($request->input('password'));
$simpan=array("fullname"=>$fullname,"email"=>$email,"password"=>$password);
DB::table('admin')->insert($simpan);
return view('welcome');
    }

    public function loginmasuk(Request $request){
    	$email=$request->input('email');
    	$password=$request->input('password');
    	
    
 $user = ModelUser2::where('email', '=', $email)->first();


    if($user){


       if(Hash::check($password,$user->password)){
                Session::put('nama',$user->fullname);
                Session::put('email',$user->email);
                Session::put('id',$user->id);
                Session::put('login',TRUE);
$item=DB::table('kiri')->where('menu2','1')->get();
$item2=DB::table('kiri')->where('menu2','2')->get();
    	return view('dalam\home',compact('item','item2'));
 
  }else{
    return redirect('/')->with('alert','Password Yang Anda Masukkan Salah');
  }

}else{

           return redirect('/')->with('alert','Username Yang Anda Masukkan Salah');
     
}


    }
    public function contents(){
       if(!Session::get('login')){
            return redirect('/')->with('alert','Anda belum login, silahkan login terlebih dahulu');
        }
        else{
    	$datakonten=DB::table('contents')->orderBy('id', 'Desc')->paginate(10);
    	 foreach($datakonten as $siswa) {
          $id=$siswa->id;
             
   
        $data=DB::table('chapter_content')->join('chapters', 'chapter_content.id_chapter', '=', 'chapters.id')->select('chapters.*', 'chapters.name')->where('chapter_content.id_content','=',$id)->get();
       
          }    
           $item=DB::table('kiri')->where('menu2','1')->get();
$item2=DB::table('kiri')->where('menu2','2')->get();
    	return view('contents\index',['datakonten'=>$datakonten],compact('data','item','item2'));
}
    }
    public function showdatakonten($id){
       if(!Session::get('login')){
            return redirect('/')->with('alert','Anda belum login, silahkan login terlebih dahulu');
        }
        else{
    	 $isidatakonten=DB::table('contents')->where('id',$id)->get();
   foreach($isidatakonten as $siswa) {
          $id=$siswa->id;
             
     
        $data=DB::table('chapter_content')->join('chapters', 'chapter_content.id_chapter', '=', 'chapters.id')->select('chapters.*', 'chapters.name')->where('chapter_content.id_content','=',$id)->get();
          }   
          $item=DB::table('kiri')->where('menu2','1')->get();
$item2=DB::table('kiri')->where('menu2','2')->get();
    $datasemua=DB::table('chapter_content')->join('chapters','chapter_content.id_chapter','=','chapters.id')->select('chapters.*','chapters.name')->get();
    return view('contents\show',compact('isidatakonten','data','datasemua','item','item2'));
}
    }
    public function editdatakonten($id){
       if(!Session::get('login')){
            return redirect('/')->with('alert','Anda belum login, silahkan login terlebih dahulu');
        }
        else{
    	 $isidatakonten=DB::table('contents')->where('id',$id)->get();
    $chapters=DB::table('chapter_content')->join('chapters', 'chapter_content.id_chapter', '=', 'chapters.id')->select('chapters.*', 'chapters.name')->where('chapter_content.id_content','=',$id)->get();
 $datasemua=DB::table('chapter_content')->join('chapters','chapter_content.id_chapter','=','chapters.id')->select('chapters.*','chapters.name')->get();
 return view('contents\edit',compact('isidatakonten','chapters','datasemua'));
}
    }

    public function simpandatakonten(Request $request){
    	
    $id_content=$request->input('id_content');
    $id_chapter=$request->input('id_chapter');
    $name=$request->input('name');
    $type=$request->input('type');
    $hide_answer=$request->input('hide_answer');
    $description=$request->input('description');
      $limit_access=$request->input('limit_access');
    if($type == 'video'){

   $file = $request->file('content_file');
 $nama_file = time()."_".$file->getClientOriginalName();
$tujuan_upload ='berkas';
$file->move($tujuan_upload,$nama_file);

       
       
    $ubahkonten=array("name"=>$name,"type"=>$type,"description"=>$description,"hide_answer"=>'1',"limit_access"=>$limit_access,"content_url"=>$nama_file);
    $ubahchapters=array('id_chapter'=>$id_chapter);
    DB::table('contents')->where('id',$id_content)->update($ubahkonten);
    DB::table('chapter_content')->where('id_content',$id_content)->update($ubahchapters);
     $item=DB::table('kiri')->where('menu2','1')->get();
$item2=DB::table('kiri')->where('menu2','2')->get();

$datakonten=DB::table('contents')->orderBy('id', 'Desc')->paginate(10);
       foreach($datakonten as $siswa) {
          $id=$siswa->id;
             
     
        $data=DB::table('chapter_content')->join('chapters', 'chapter_content.id_chapter', '=', 'chapters.id')->select('chapters.*', 'chapters.name')->where('chapter_content.id_content','=',$id)->get();
      
          }    
    return view('contents\index',['datakonten'=>$datakonten],compact('data','item','item2'));
}

if($type == 'pdf'){
     $file = $request->file('content_file');
 $nama_file = time()."_".$file->getClientOriginalName();
$tujuan_upload ='berkas';
$file->move($tujuan_upload,$nama_file);

        $deskripsi=$request->input('description');
        $limit_access=$request->input('limit_access');
    $ubahkonten=array('name'=>$name,'type'=>$type,'description'=>$description,"hide_answer"=>'1','limit_access'=>$limit_access,"content_url"=>$nama_file);
    $ubahchapters=array('id_chapter'=>$id_chapter);
    DB::table('contents')->where('id',$id_content)->update($ubahkonten);
    DB::table('chapter_content')->where('id_content',$id_content)->update($ubahchapters);
     $item=DB::table('kiri')->where('menu2','1')->get();
$item2=DB::table('kiri')->where('menu2','2')->get();

$datakonten=DB::table('contents')->orderBy('id', 'Desc')->paginate(10);
       foreach($datakonten as $siswa) {
          $id=$siswa->id;
             
     
        $data=DB::table('chapter_content')->join('chapters', 'chapter_content.id_chapter', '=', 'chapters.id')->select('chapters.*', 'chapters.name')->where('chapter_content.id_content','=',$id)->get();
      
          }    
      return view('contents\index',['datakonten'=>$datakonten],compact('data','item','item2'));
}
if($type=='quiz'){
   $ubahkonten=array('name'=>$name,'type'=>$type,'description'=>$description,"hide_answer"=>'1','limit_access'=>$limit_access);
   $ubahchapters=array('id_chapter'=>$id_chapter);
   DB::table('contents')->where('id',$id_content)->update($ubahkonten);
   DB::table('chapter_content')->where('id_content',$id_content)->update($ubahchapters);
  $datakonten=DB::table('contents')->orderBy('id', 'Desc')->paginate(10);
       foreach($datakonten as $siswa) {
          $id=$siswa->id;
             
     
        $data=DB::table('chapter_content')->join('chapters', 'chapter_content.id_chapter', '=', 'chapters.id')->select('chapters.*', 'chapters.name')->where('chapter_content.id_content','=',$id)->get();
      
          }    
            $item=DB::table('kiri')->where('menu2','1')->get();
$item2=DB::table('kiri')->where('menu2','2')->get();
      return view('contents\index',['datakonten'=>$datakonten],compact('data','item','item2'));
}

  

    }
    public function createdata(){
       if(!Session::get('login')){
            return redirect('/')->with('alert','Anda belum login, silahkan login terlebih dahulu');
        }
        else{
    	$datasemua=DB::table('chapter_content')->join('chapters','chapter_content.id_chapter','=','chapters.id')->select('chapters.*','chapters.name')->get();
    	return view('contents\create',compact('datasemua'));
    }

    }
    public function caridata(Request $request){
       if(!Session::get('login')){
            return redirect('/')->with('alert','Anda belum login, silahkan login terlebih dahulu');
        }
        else{
    	$q=$request->input('q');
    	$datakonten=DB::table('contents')->where('name','like',"%".$q."%")->paginate(100);
         foreach($datakonten as $siswa) {
          $id=$siswa->id;
             
     
        $data=DB::table('chapter_content')->join('chapters', 'chapter_content.id_chapter', '=', 'chapters.id')
            ->select('chapters.*', 'chapters.name')->where('chapter_content.id_content','=',$id)->get();
          }   
    	return view('contents\index',['datakonten' => $datakonten],compact('data'));
    }
    }
    public function simpandatabarukonten(Request $request){
    	$id_chapter=$request->input('id_chapter');
    	$name=$request->input('name');
    	$description=$request->input('description');
    	$type=$request->input('type');
    	$limit_access=$request->Input('limit_access');
    	$kkm=$request->input('kkm');
    	$hide_answer=$request->input('hide_answer');
    
    	$length=$request->input('length');

 if($type == 'video'){
 
 $file = $request->file('content_file');
 $nama_file = time()."_".$file->getClientOriginalName();
$tujuan_upload ='berkas';
$file->move($tujuan_upload,$nama_file);

  $simpankonten=array("name"=>$name,"type"=>$type,"description"=>$description,"hide_answer"=>'1',"limit_access"=>$limit_access,"content_url"=>$nama_file,'order_number'=>'2','length'=>'1','hls_video_url'=>'0','kkm'=>$kkm);
 
    DB::table('contents')->insert($simpankonten);
     $id_content1=DB::table('contents')->where('name',$name)->get();
     foreach($id_content1 as $p){
      $id_content=$p->id;
     }
    $simpanchapter=array('id_chapter'=>$id_chapter,'id_content'=>$id_content);
    DB::table('chapter_content')->insert($simpanchapter);
  
$datakonten=DB::table('contents')->orderBy('id', 'Desc')->paginate(10);
       foreach($datakonten as $siswa) {
          $id=$siswa->id;
             
     
        $data=DB::table('chapter_content')->join('chapters', 'chapter_content.id_chapter', '=', 'chapters.id')->select('chapters.*', 'chapters.name')->where('chapter_content.id_content','=',$id)->get();
      
          }    
           $item=DB::table('kiri')->where('menu2','1')->get();
$item2=DB::table('kiri')->where('menu2','2')->get();

    return view('contents\index',['datakonten'=>$datakonten],compact('data','item','item2'));

  } if($type=='pdf'){
    
 $file = $request->file('content_file');
 $nama_file = time()."_".$file->getClientOriginalName();
$tujuan_upload ='berkas';
$file->move($tujuan_upload,$nama_file);

  $simpankonten=array("name"=>$name,"type"=>$type,"description"=>$description,"hide_answer"=>'1',"limit_access"=>$limit_access,"content_url"=>$nama_file,'order_number'=>'2','length'=>'1','hls_video_url'=>'0','kkm'=>$kkm);
 
    DB::table('contents')->insert($simpankonten);
     $id_content1=DB::table('contents')->where('name',$name)->get();
     foreach($id_content1 as $p){
      $id_content=$p->id;
     }
    $simpanchapter=array('id_chapter'=>$id_chapter,'id_content'=>$id_content);
    DB::table('chapter_content')->insert($simpanchapter);
  
$datakonten=DB::table('contents')->orderBy('id', 'Desc')->paginate(10);
       foreach($datakonten as $siswa) {
          $id=$siswa->id;
             
     
        $data=DB::table('chapter_content')->join('chapters', 'chapter_content.id_chapter', '=', 'chapters.id')->select('chapters.*', 'chapters.name')->where('chapter_content.id_content','=',$id)->get();
      
          }    
           $item=DB::table('kiri')->where('menu2','1')->get();
$item2=DB::table('kiri')->where('menu2','2')->get();

    return view('contents\index',['datakonten'=>$datakonten],compact('data','item','item2'));



  }
  if($type='quiz'){
     $simpankonten=array("name"=>$name,"type"=>$type,"description"=>$description,"hide_answer"=>'1',"limit_access"=>$limit_access,"content_url"=>'quiz','order_number'=>'2','length'=>'1','hls_video_url'=>'0','kkm'=>$kkm);
 
    DB::table('contents')->insert($simpankonten);
     $id_content1=DB::table('contents')->where('name',$name)->get();
     foreach($id_content1 as $p){
      $id_content=$p->id;
     }
    $simpanchapter=array('id_chapter'=>$id_chapter,'id_content'=>$id_content);
    DB::table('chapter_content')->insert($simpanchapter);
  
$datakonten=DB::table('contents')->orderBy('id', 'Desc')->paginate(10);
       foreach($datakonten as $siswa) {
          $id=$siswa->id;
             
     
        $data=DB::table('chapter_content')->join('chapters', 'chapter_content.id_chapter', '=', 'chapters.id')->select('chapters.*', 'chapters.name')->where('chapter_content.id_content','=',$id)->get();
      
          }    
           $item=DB::table('kiri')->where('menu2','1')->get();
$item2=DB::table('kiri')->where('menu2','2')->get();

    return view('contents\index',['datakonten'=>$datakonten],compact('data','item','item2'));


  }
 
}
public function quiznew(){
  return view('contents\quiz');
}
public function keluar(Request $request){
   Session::flush();
         Session::regenerate();
         return view('welcome');
}


public function editquiz($id){
  $datacontent=DB::table('contents')->where('id',$id)->get();
     $item=DB::table('kiri')->where('menu2','1')->get();
$item2=DB::table('kiri')->where('menu2','2')->get();

  return view('contents\editquiz',compact('datacontent','item','item2'));
}
}
